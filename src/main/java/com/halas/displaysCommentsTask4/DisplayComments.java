package com.halas.displaysCommentsTask4;

import com.halas.menuView.Functional;
import com.halas.menuView.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public final class DisplayComments {
    private static final Scanner INPUT = new Scanner(System.in);
    private static final File DEFAULT_FILE_NAME = new File(
            "C:\\Java Projects\\task10_IO_NIO\\resource\\Java-source\\Map.txt");
    private static final String JAVA_DOC_COMMENT_START = "/**";
    private static final String STAR_COMMENT_START = "/*";
    private static final String LINE_COMMENT = "//";
    private static final String ENDING_JAVA_DOC_AND_STAR_COMMENT = "*/";


    private static final Logger LOG = LogManager.getLogger(DisplayComments.class);
    private static File mainFile = new File(String.valueOf(DEFAULT_FILE_NAME));

    /**
     * isComment Star - це коментар типу /*.
     */
    private static boolean isCommentStar = false;
    /**
     * isComment Slash - це коментар типу //.
     * */
    private static boolean isCommentSlash = true;
    /**
     * isDocumentation - це опис типу /**.
     * */
    private static boolean isDocumentation = false;


    /**
     * Заготовлені файли з java кодом.
     */
    private static File[] files;


    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;


    /**
     * Конструктор без параметрів, з ініцілізацією об'єктів.
     */
    private DisplayComments() {
        files = mainFile.getParentFile().listFiles();
        initMenu();
    }


    /**
     * Винесено окремим методом оскільки справа значення
     * будуть мінятись щоразу при воводі меню.
     */
    private void dynamicChangeMenu() {
        menu.put("1", " 1 - Change file, current file: "
                + mainFile.getName() + ".");
        menu.put("2", " 2 - Change display documentations: "
                + isDocumentation + ".");
        menu.put("3", " 3 - Change display \"/**/\" comments: "
                + isCommentStar + ".");
        menu.put("4", " 4 - Change display \"\\\\\" comments: "
                + isCommentSlash + ".");
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        dynamicChangeMenu();
        menu.put("5", " 5 - Start reading from file.");
        menu.put("6", " 6 - Back to task menu.");

        methodsMenu.put("1", this::changeFile);
        methodsMenu.put("2", this::changeDocumentation);
        methodsMenu.put("3", this::changeCommentStar);
        methodsMenu.put("4", this::changeCommentSlash);
        methodsMenu.put("5", this::beginRead);
        methodsMenu.put("6", Menu::runMenu);

    }

    private void changeCommentStar() {
        isCommentStar = !isCommentStar;
    }

    private void changeCommentSlash() {
        isCommentSlash = !isCommentSlash;
    }

    private void changeDocumentation() {
        isDocumentation = !isDocumentation;
    }

    private void outputMenu() {
        dynamicChangeMenu();
        System.out.println("\nMENU DISPLAY COMMENTS:");
        menu.values().forEach(System.out::println);
    }

    private void showMenuSetting() {
        int size = files != null ? files.length : 0;
        System.out.println("\nMENU SETTING FILE:");
        for (int i = 0; i < size; i++) {
            System.out.println(" " + (i + 1) + " - Set file "
                    + files[i].getName() + ".");
        }
    }

    private void setFile(int index) {
        int size = files.length;
        //бо індекси на одиницю менше від меню вибору
        index--;
        for (int i = 0; i < size; i++) {
            if (index == i) {
                mainFile = files[i];
                if (LOG.isTraceEnabled()) {
                    LOG.trace("File successfully set!");
                }
                return;
            }
        }
        if (LOG.isTraceEnabled()) {
            LOG.trace("Can't set file!");
        }
    }

    private void changeFile() {
        int value;
        while (true) {
            try {
                System.out.println("\n");
                showMenuSetting();
                if (LOG.isInfoEnabled()) {
                    LOG.info("Please, select menu point.");
                }
                String inputInt = INPUT.nextLine();
                value = Integer.parseInt(inputInt);
                setFile(value);

                break;
            } catch (IllegalArgumentException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Size can't be less then zero");
                }
            } catch (Exception e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Please try again!\n");
                }
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Chose file(index): " + value);
        }
    }

    private void beginRead() throws IOException {
        if (isDocumentation) {
            readByStar(JAVA_DOC_COMMENT_START);
        }
        if (isCommentStar) {
            readByStar(STAR_COMMENT_START);
        }
        if (isCommentSlash) {
            readSlashComment();
        }
    }


    private void readSlashComment() throws IOException {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(mainFile),
                        StandardCharsets.UTF_8))) {

            StringBuilder st = new StringBuilder();
            while (br.read() != -1) {
                String line = br.readLine();
                if (line != null) {
                    if (line.trim().contains(LINE_COMMENT)) {
                        line = deleteFirstSymbolsBeforeComment(line, LINE_COMMENT);
                        st.append(line.trim())
                                .append("\n");
                    }
                }
            }
            if (LOG.isDebugEnabled()) {
                LOG.info(st);
            }
        }
    }

    private String deleteFirstSymbolsBeforeComment(String line, String comment) {
        int size = line.length();
        while (!line.startsWith(comment)) {
            //delete first symbol, coz comment can't be after words
            line = line.substring(1, size);
            size--;
        }
        return line;
    }

    private void readComments(BufferedReader br,
                              String line,
                              StringBuilder text,
                              String typeComment) throws IOException {

        line = deleteFirstSymbolsBeforeComment(line, typeComment);
        do {
            if (line.contains(ENDING_JAVA_DOC_AND_STAR_COMMENT)) {
                //кінець коменту і видалення все що перед ним
                text.append(deleteFirstSymbolsBeforeComment(
                        line, ENDING_JAVA_DOC_AND_STAR_COMMENT))
                        .append("\n");
                break;
            }
            text.append(line)
                    .append("\n");
        } while ((line = br.readLine()) != null);
    }

    private void readByStar(String typeComment) throws IOException {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(mainFile)))) {

            StringBuilder text = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains(typeComment)) {
                    if (typeComment.equals(JAVA_DOC_COMMENT_START)) {
                        readComments(br, line, text, typeComment);

                        //якщо хочу знайти /* але без джава доку /**
                    } else if (!line.contains(JAVA_DOC_COMMENT_START)) {
                        readComments(br, line, text, typeComment);
                    }
                }
            }
            if (LOG.isInfoEnabled()) {
                LOG.info(text);
            }
        }
    }

    public static void run() {
        String key;
        DisplayComments menu = new DisplayComments();
        while (true) {
            try {
                System.out.println("\n");
                menu.outputMenu();
                if (LOG.isInfoEnabled()) {
                    LOG.info("Please, select menu point.");
                }
                key = INPUT.nextLine();
                menu.methodsMenu.get(key).start();
            } catch (NullPointerException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nError\nPlease choose correct point.");
                }
            } catch (IOException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nFile error!");
                }
            } catch (Exception e) {
                if (LOG.isFatalEnabled()) {
                    LOG.fatal("\nFatal error");
                }
            }
        }
    }
}
