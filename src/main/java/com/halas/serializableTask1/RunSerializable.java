package com.halas.serializableTask1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class RunSerializable implements AutoCloseable {


    private static final Logger LOG = LogManager.getLogger(RunSerializable.class);
    private static final String DEFAULT_PATH_FILES
            = "C:\\Java Projects\\task10_IO_NIO\\resource\\Serializable\\";
    private static final int MAGIC_AGE = 51;

    /**
     * запис у файл (serializable) записує стан об'єкту.
     */
    private static void serializableInFile(
            final String fileName,
            final Object obj) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(fileName);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(obj);
            oos.flush();
        }
    }

    /**
     * зчитування з файлу (Десеріалізація) зчитує стан об'єкту.
     */
    private static Object serializableFromFile(final String fileName)
            throws IOException, ClassNotFoundException {
        try (FileInputStream fin = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(fin)) {
            return ois.readObject();
        }

    }

    public static void runTask1() throws IOException, ClassNotFoundException {
        final String fatherFileName = DEFAULT_PATH_FILES
                + "SerializableFather.txt";
        final String sonFileName = DEFAULT_PATH_FILES
                + "SerializableSon.txt";

        Father father = new Father(
                "Luba",
                MAGIC_AGE,
                new ArrayList<>(Arrays.asList("Bohdan", "Roman")),
                "Vol");
        Son son = new Son();

        if (LOG.isTraceEnabled()) {
            LOG.trace("Father before: " + father);
            LOG.trace("Son before: " + son);
        }
        if (LOG.isInfoEnabled()) {
            //статичні і файнал поля не можна серіалізувати
            LOG.info("In Father transient is: ownName");
            LOG.info("In Son transient is: age\n");
        }

        serializableInFile(fatherFileName, father);
        Father father1 = (Father) serializableFromFile(fatherFileName);


        serializableInFile(sonFileName, son);
        Son son1 = (Son) serializableFromFile(sonFileName);

        if (LOG.isInfoEnabled()) {
            LOG.info("Object father(after): " + father1);
            LOG.info("Object sun(after): " + son1);
        }
    }

    @Override
    public void close() {
        if (LOG.isInfoEnabled()) {
            LOG.info("Closed files");
        }
    }
}
