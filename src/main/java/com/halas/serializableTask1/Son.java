package com.halas.serializableTask1;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Son implements Serializable {
    private static transient String DEFAULT_OWN_NAME = "Yurii";
    @SuppressWarnings("FieldCanBeLocal")
    private transient int defaultAge = 19;
    private final transient List<String> defaultBrothers
            = new LinkedList<>(Collections.singletonList("Andrew"));
    private static final String DEFAULT_NAME_OF_PARENTS = "Natalie, Volodymyr";
    static final long serialVersionUID = 12374548L;


    @Override
    public String toString() {
        return "Son{"
                + "nameOfParents='" + DEFAULT_NAME_OF_PARENTS
                + '\'' + ", (just transient)age=" + defaultAge
                + ", (final transient)brothers=" + defaultBrothers
                + ", (static transient)ownName='" + DEFAULT_OWN_NAME
                + '\''
                + '}';
    }
}
