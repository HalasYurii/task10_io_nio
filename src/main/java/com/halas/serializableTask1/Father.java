package com.halas.serializableTask1;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Father implements Serializable {
    private String nameOfParents;
    private int age;
    private List<String> brothers;
    private transient String ownName;
    static final long serialVersionUID = 1237549L;

    Father(final String nameOfParents,
           final int age,
           final List<String> brothers,
           final String ownName) {

        this.brothers = new LinkedList<>();
        this.nameOfParents = nameOfParents;
        this.age = age;
        this.brothers = brothers;
        this.ownName = ownName;
    }

    @Override
    public String toString() {
        return "Father{"
                + "nameOfParents='"
                + nameOfParents + '\''
                + ", age=" + age
                + ", brothers=" + brothers
                + ", (transient)ownName='" + ownName + '\''
                + '}';
    }
}
