package com.halas.menuView;


@FunctionalInterface
public interface Functional {
    void start() throws Exception;
}
