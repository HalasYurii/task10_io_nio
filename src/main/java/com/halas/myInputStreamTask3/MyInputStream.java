package com.halas.myInputStreamTask3;

import java.io.InputStream;
import java.io.PushbackInputStream;

public class MyInputStream extends PushbackInputStream {
    public MyInputStream(InputStream in, int size) {
        super(in, size);
    }

    public MyInputStream(InputStream in) {
        super(in);
    }
}
