package com.halas.myInputStreamTask3;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Arrays;

public class Manipulations {
    private static final Logger LOG = LogManager.getLogger(Manipulations.class);
    private static final String DEFAULT_SENTENCE = "I raise my flags, don my clothes";
    private static final int DEFAULT_SIZE_BUFFER = 1024;


    private static void readByPushBack(
            byte[] buffer, PushbackInputStream pis) throws IOException {

        int avail = pis.read(buffer);
        while (avail > 0) {
            avail = pis.read();
        }

        System.out.println("\n");

        if (LOG.isInfoEnabled()) {
            LOG.info("***ReadByPushBack***");
            LOG.info("Available: " + pis.available());
            LOG.info("Sentence: " + new String(buffer));
        }
    }

    private static void unReadByPushBack(
            byte[] buffer, PushbackInputStream pis, String sentence) throws IOException {

        pis.unread(sentence.getBytes());
        int avail = pis.read(buffer);
        while (avail > 0) {
            avail = pis.read();
        }

        System.out.println("\n");

        if (LOG.isInfoEnabled()) {
            LOG.info("***UNReadByPushBack***");
            LOG.info("Available: " + pis.available());
            LOG.info("Sentence: " + new String(buffer));
        }
    }

    public static void run() {
        try {
            byte[] buffer = new byte[DEFAULT_SIZE_BUFFER];
            String mySentence = "New String to test pushBack";
            String mySentence2 = "Second sentence trying..";

            InputStream is = new ByteArrayInputStream(DEFAULT_SENTENCE.getBytes());
            MyInputStream pis = new MyInputStream(is, DEFAULT_SENTENCE.length());

            readByPushBack(buffer, pis);
            unReadByPushBack(buffer, pis, mySentence2);
            readByPushBack(buffer, pis);
            unReadByPushBack(buffer, pis, mySentence);


        } catch (Exception e) {
            if (LOG.isFatalEnabled()) {
                LOG.fatal("Need more practice in programming!");
            }
        }
    }
}
