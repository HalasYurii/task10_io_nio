package com.halas.showContentsDirectoriesTask5;

import com.halas.menuView.Functional;
import com.halas.menuView.Menu;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.util.*;
import java.io.File;

public final class Directories {
    private static final Scanner INPUT = new Scanner(System.in);
    private static final File DEFAULT_ROOT = new File("C:\\Java Projects");
    private static final Logger LOG = LogManager.getLogger(Directories.class);

    /**
     * For menu to go back =0, and set parent =1 .
     */
    private static final int BACK = 0;
    private static final int PARENT_INFO = 1;

    /**
     * ���� ������� File file = new File("C:");
     * �� ���� ������ �������� ������� ��������
     * ��� �� ���� ������ file.getParent().
     * <p>
     * File file = new File("C:");
     * LOG.debug(file.getAbsolutePath());
     * result: "C:\Java Projects\task10_IO_NIO"
     */
    private static final String NOT_ALLOWED_SYM = "C:";

    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;


    private static File root;
    private static File[] filesDir;


    private Directories() {
        root = DEFAULT_ROOT;
        setFilesDirCurrentRoot();
        initMenu();
    }

    private void setFilesDirCurrentRoot() {
        filesDir = Arrays.stream(Objects.requireNonNull(root.listFiles()))
                .filter(File::isDirectory)
                .toArray(File[]::new);
    }

    private void dynamicSetRoot() {
        menu.put("1", " 1 - Set new root, current root: "
                + root.getAbsolutePath() + ".");
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        dynamicSetRoot();
        menu.put("2", " 2 - Cd root(change directory by inputted).");
        menu.put("3", " 3 - Dir current root.");
        menu.put("4", " 4 - Show everything in current root.");
        menu.put("5", " 5 - Back to task menu.");

        methodsMenu.put("1", this::changeRoot);
        methodsMenu.put("2", this::cdRootByInputted);
        methodsMenu.put("3", this::dirCurrentRoot);
        methodsMenu.put("4", this::showEverything);
        methodsMenu.put("5", Menu::runMenu);

    }

    private void showMenu() {
        dynamicSetRoot();
        System.out.println("\nMENU DIRECTORIES AND FILES:");
        menu.values().forEach(System.out::println);
    }

    private void showMenuRoot() {
        setFilesDirCurrentRoot();
        String parent;
        if (root.getParentFile() == null) {
            parent = "does not exist!";
        } else {
            parent = root.getParent();
        }
        System.out.println("\nMENU SETTING ROOT:");

        if (LOG.isInfoEnabled()) {
            LOG.info("Current root: " + root.getAbsolutePath());
        }
        System.out.println(" 0 - Back.");
        System.out.println(" 1 - Go to parent -> " + parent + ".");
        int size = filesDir.length;
        for (int i = 0; i < size; i++) {
            System.out.println(" " + (i + 2) + " - " + filesDir[i].getName());
        }
    }

    private void setFile(int index) {
        if (index == PARENT_INFO) {
            if (root.getParent() == null) {
                if (LOG.isFatalEnabled()) {
                    LOG.fatal("Parent does not exist!");
                }
            } else {
                root = root.getParentFile();
                if (LOG.isInfoEnabled()) {
                    LOG.info("Successfully set parent.");
                }
            }
            return;
        }
        int size = filesDir.length;

        //�� ������� �� ������� ����� �� ���� ������
        index -= 2;
        for (int i = 0; i < size; i++) {
            if (index == i) {
                root = new File(filesDir[i].toString() + "\\");
                if (LOG.isInfoEnabled()) {
                    LOG.info("File successfully set!");
                    return;
                }
            }
        }
        if (LOG.isInfoEnabled()) {
            LOG.info("Can't set file!");
        }
    }

    private void changeRoot() {
        int value;
        while (true) {
            try {
                System.out.println("\n");
                showMenuRoot();
                if (LOG.isInfoEnabled()) {
                    LOG.info("Please, select menu point.");
                }
                String inputInt = INPUT.nextLine();
                value = Integer.parseInt(inputInt);
                if (value == BACK) {
                    break;
                }
                setFile(value);

            } catch (IllegalArgumentException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Size can't be less then zero");
                }
            } catch (Exception e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Please try again!\n");
                }
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Chose file(index): " + value);
        }
    }

    private void cdRootByInputted() {
        if (LOG.isInfoEnabled()) {
            LOG.info("Enter new root:\n");
        }
        String fName = INPUT.nextLine();
        File newFile = new File(fName);
        if (newFile.isDirectory() && (!fName.equals(NOT_ALLOWED_SYM))) {
            root = newFile;
            if (LOG.isInfoEnabled()) {
                LOG.info("Successfully set!");
            }
        } else {
            if (LOG.isErrorEnabled()) {
                LOG.error("Can't set this file to root: " + newFile + ".");
            }
        }
    }

    private void dirCurrentRoot() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Current file: " + root.getAbsolutePath() + ".");
        }
        if (LOG.isTraceEnabled()) {
            LOG.trace("List files: ");
        }
        File[] files = root.listFiles();
        Arrays.stream(files != null ? files : new File[0]).forEach(LOG::info);
    }

    private void showEverything() {
        showByRecursion(root);
    }

    private static void showByRecursion(File file) {
        if (file.isFile()) {
            if (LOG.isInfoEnabled()) {
                LOG.info("FILE: " + file.getAbsolutePath());
            }
        } else if (file.isDirectory()) {
            Arrays.stream(Objects.requireNonNull(file.listFiles()))
                    .forEach(Directories::showByRecursion);
        } else {
            if (LOG.isErrorEnabled()) {
                LOG.error("Problems!");
            }
        }
    }

    public static void run() {
        String key;
        Directories menu = new Directories();
        while (true) {
            try {
                System.out.println("\n");
                menu.showMenu();
                if (LOG.isInfoEnabled()) {
                    LOG.info("Please, select menu point.");
                }
                key = INPUT.nextLine();
                System.out.println("\n\n");
                menu.methodsMenu.get(key).start();
            } catch (NullPointerException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nError\nPlease choose correct point.");
                }
            } catch (IOException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nFile error!");
                }
            } catch (Exception e) {
                if (LOG.isFatalEnabled()) {
                    LOG.fatal("\nFatal error");
                }
            }
        }
    }
}
