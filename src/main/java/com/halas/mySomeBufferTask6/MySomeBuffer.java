package com.halas.mySomeBufferTask6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public final class MySomeBuffer {
    private static final File DEFAULT_FILE = new File(
            "C:\\Java Projects\\task10_IO_NIO\\resource\\SomeBuffer\\Yura.txt");
    private static final Logger LOG = LogManager.getLogger(MySomeBuffer.class);
    private static final int DEFAULT_SIZE = 3072;

    private FileChannel channel;
    private RandomAccessFile file;

    private MySomeBuffer() {
        super();
        setDefaultFile();
        channel = file.getChannel();
    }

    private void setDefaultFile() {
        try {
            this.file = new RandomAccessFile(DEFAULT_FILE, "rw");
        } catch (FileNotFoundException e) {
            if (LOG.isFatalEnabled()) {
                LOG.fatal("Wrong default file!");
            }
        }
    }

    private String readUsingChannel() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(DEFAULT_SIZE);

        channel.position(0);
        int numRead = channel.read(buffer);

        if (numRead == -1) {
            if (LOG.isFatalEnabled()) {
                LOG.fatal("File is empty!");
                return "";
            }
        }
        byte[] data = new byte[numRead];
        System.arraycopy(buffer.array(), 0, data, 0, numRead);
        return new String(data);
    }

    @SuppressWarnings("UnusedReturnValue")
    private boolean writeUsingChannel(String message) throws IOException {
        message = message.trim();
        if (message.isEmpty()) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Message is empty!");
            }
            return false;
        }
        channel = file.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(message.getBytes().length);
        buffer.put(message.getBytes());

        buffer.flip();
        channel.write(buffer);

        if (LOG.isInfoEnabled()) {
            LOG.info("Message successfully wrote to file: ");
        }
        return true;
    }

    public static void run() {
        try {
            MySomeBuffer someBuffer = new MySomeBuffer();

            String mess1 = "First trying to write in file!";
            String mess2 = "Second trying to set new String in file";

            if (LOG.isInfoEnabled()) {
                LOG.info("*****MySomeBuffer*****\n\n");
                LOG.info("Write to file message: " + mess1);
            }

            someBuffer.writeUsingChannel(mess1);

            if (LOG.isInfoEnabled()) {
                LOG.info("Read from file... ");
            }

            String readMess = someBuffer.readUsingChannel();

            if (LOG.isInfoEnabled()) {
                LOG.info("Text from file: " + readMess);
                LOG.info("Write to file message: " + mess2);
            }

            someBuffer.writeUsingChannel(mess2);

            if (LOG.isInfoEnabled()) {
                LOG.info("Read from file: ");
            }

            readMess = someBuffer.readUsingChannel();

            if (LOG.isInfoEnabled()) {
                LOG.info("Text from file: " + readMess);
            }

            if (LOG.isDebugEnabled()) {
                LOG.debug("End successfully!");
            }
        } catch (IOException e) {
            if (LOG.isErrorEnabled()) {
                LOG.error("IOException..");
            }
        } catch (Exception e) {
            if (LOG.isFatalEnabled()) {
                LOG.fatal("Unknown Exception!");
            }
        }
    }
}
