package com.halas.newIOTask7;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;


public class Client {
    private static final int PORT = 9090;
    private static final String ADDRESS = "localhost";
    private static final Logger LOG = LogManager.getLogger(Client.class);
    private static final Scanner INPUT = new Scanner(System.in);

    private void startClient()
            throws IOException, InterruptedException {
        InetSocketAddress hostAddress = new InetSocketAddress(ADDRESS, PORT);
        SocketChannel client = SocketChannel.open(hostAddress);
        if (LOG.isInfoEnabled()) {
            LOG.info("Client... started");
        }

        String threadName = Thread.currentThread().getName();
        String leftName = threadName + " left channel!";

        String message;

        ByteBuffer buffer = ByteBuffer.allocate(3072);
        do {
            Thread n = new Thread(()-> {
                try {
                    ByteBuffer b = ByteBuffer.allocate(3072);
                    client.read(b);

                    String mess = new String(b.array());
                    LOG.debug(mess);
                }catch (IOException e) {
                    LOG.error("Can't read from server\n"+e.getClass());
                }
            });
            n.start();

            buffer.clear();
            if (LOG.isInfoEnabled()) {
                LOG.info("Input your message");
            }

            message = INPUT.nextLine();
            // Send messages to server
            if (message.equals("EXIT")) {
                break;
            } else {
                message = threadName + ": " + message;
            }
            buffer = ByteBuffer.wrap(message.getBytes());
            client.write(buffer);



        } while (true);
        ByteBuffer br = ByteBuffer.wrap(leftName.getBytes());
        client.write(br);
        br.clear();

        client.close();
        if (LOG.isDebugEnabled()) {
            LOG.debug(leftName);
        }
    }

    public static void main(String[] args) {
        String name = "DefaultClient";
        try {
            if (LOG.isInfoEnabled()) {
                LOG.info("Input your name: ");
            }
            name = INPUT.nextLine();
            if (LOG.isInfoEnabled()) {
                Thread.currentThread().setName(name);
            }
            new Client().startClient();
        } catch (Exception e) {
            if (LOG.isFatalEnabled()) {
                LOG.fatal("Something wrong with client..." + name);
            }
        }
    }
}