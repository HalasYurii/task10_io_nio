package com.halas.newIOTask7;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;

public class Server {
    private static final int PORT = 9090;
    private static final String ADDRESS = "localhost";
    private static final Logger LOG = LogManager.getLogger(Server.class);

    private Selector selector;
    private Map<SocketChannel, List> dataMapper;
    private InetSocketAddress listenAddress;
    private volatile String messageForClient;

    private Server(String address, int port) {
        listenAddress = new InetSocketAddress(address, port);
        dataMapper = new HashMap<>();
        messageForClient = "null";
    }

    public static void runNIO() {
        if (LOG.isInfoEnabled()) {
            LOG.info("Sorry, there are no available code for start this program from current main!");
        }
    }

    // create server channel
    private void startServer() throws IOException, InterruptedException {
        this.selector = Selector.open();
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);

        // retrieve server socket and bind to port
        serverChannel.socket().bind(listenAddress);
        serverChannel.register(this.selector, SelectionKey.OP_ACCEPT);

        if (LOG.isInfoEnabled()) {
            LOG.info("Server started...");
        }
        boolean isRead = false;
        while (true) {
            // wait for events
            this.selector.select();

            //work on selected keys
            Iterator keys = this.selector.selectedKeys().iterator();
            while (keys.hasNext()) {
                SelectionKey key = (SelectionKey) keys.next();

                // this is necessary to prevent the same key from coming up
                // again the next time around.
                keys.remove();

                if (!key.isValid()) {
                    continue;
                }

                if (key.isAcceptable()) {
                    this.accept(key);
                } else if ((!isRead) && key.isReadable()) {
                    this.read(key);
                    isRead = true;
                } else if (isRead && key.isWritable()) {
                    this.write(key);
                    isRead = false;
                }
            }
        }
    }

    //accept a connection made to this channel's socket
    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();
        SocketChannel channel = serverChannel.accept();
        channel.configureBlocking(false);
        Socket socket = channel.socket();
        SocketAddress remoteAddr = socket.getRemoteSocketAddress();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Connected to: " + remoteAddr);
        }
        // register channel with selector
        channel.register(this.selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
        dataMapper.put(channel, new ArrayList());
    }

    private void write(SelectionKey key) throws IOException, InterruptedException {

        for (SocketChannel sChannel : dataMapper.keySet()) {
            ByteBuffer buffer = ByteBuffer.wrap(this.messageForClient.getBytes());
            sChannel.write(buffer);
        }
    }

    //read from the socket channel
    private void read(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        channel.blockingLock();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int numRead;
        numRead = channel.read(buffer);

        if (numRead == -1) {
            this.dataMapper.remove(channel);
            Socket socket = channel.socket();
            SocketAddress remoteAddr = socket.getRemoteSocketAddress();
            if (LOG.isInfoEnabled()) {
                LOG.info("Connection closed by client: " + remoteAddr);
            }
            channel.close();
            key.cancel();
            return;
        }

        byte[] data = new byte[numRead];
        System.arraycopy(buffer.array(), 0, data, 0, numRead);
        String mess = new String(data);
        this.messageForClient = mess;

        if (LOG.isDebugEnabled()) {
            LOG.debug(mess);
        }
    }

    /**
     * ��������� ������.
     */
    public static void main(String[] args) {
        try {
            new Server(ADDRESS, PORT).startServer();
        } catch (IOException e) {
            if (LOG.isErrorEnabled()) {
                LOG.error("Exception in server!");
            }
        } catch (Exception e) {
            if (LOG.isFatalEnabled()) {
                LOG.fatal("Server crushed!!");
            }
        }
    }
}
