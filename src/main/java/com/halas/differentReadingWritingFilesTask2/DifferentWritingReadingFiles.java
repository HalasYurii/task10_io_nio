package com.halas.differentReadingWritingFilesTask2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Scanner;

import static com.halas.differentReadingWritingFilesTask2.MenuBuffer.runMenuBuffer;

public class DifferentWritingReadingFiles implements AutoCloseable {
    private static final Scanner INPUT = new Scanner(System.in);
    private static File mainFile;
    private static int sizeBuffer;
    private static final Logger LOG = LogManager.getLogger(
            DifferentWritingReadingFiles.class);
    private static File[] files;

    /**
     * Вкрав у декларації дофолт розмір BufferedReader буфера.
     */
    private static final int DEFAULT_SIZE_BUFFER = 8192;
    private static final String DEFAULT_PACKAGE_FILES
            = "C:\\Java Projects\\task10_IO_NIO\\resource\\ReadWriteTask2";
    private static final File DEFAULT_FILE_NAME = new File(
            DEFAULT_PACKAGE_FILES + "\\4mb.pdf");
    private static final long CONVERT_TO_SEC = 1_000_000_000;
    private static final String WORST_FILES_ENDING = ".pdf";

    /**
     * Змінна закриття файлу.
     * У функції writeToFileFromConsole.
     */
    private static final String CLOSE_WRITING = "END";

    static void readByByte() throws IOException {

        try (InputStream iStream = new FileInputStream(mainFile)) {
            long begin = System.nanoTime();
            int content = iStream.read();
            while (content != -1) {
                content = iStream.read();
            }
            long end = System.nanoTime();
            double result = (end - begin * 1.) / CONVERT_TO_SEC;
            if (LOG.isInfoEnabled()) {
                LOG.info("Current file: " + mainFile.getName());
                LOG.info("Size of buffer: " + sizeBuffer);
                LOG.info("(ByByte)Result in seconds: " + result);
            }
        }
    }

    static void readByBufferedReaderInInputStream() throws IOException {
        setBufferSize();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(mainFile),
                        StandardCharsets.UTF_8), sizeBuffer)) {

            long begin = System.nanoTime();
            int content = br.read();
            while (content != -1) {
                content = br.read();
            }
            long end = System.nanoTime();
            double result = (end - begin * 1.) / CONVERT_TO_SEC;
            if (LOG.isInfoEnabled()) {
                LOG.info("Current file: " + mainFile.getName());
                LOG.info("Size of buffer: " + sizeBuffer);
                LOG.info("(BufferedReader in InputStream)Result in seconds: "
                        + result);
            }
        }
    }


    private static void createNewFile() {
        while (true) {
            try {
                System.out.println("\n\n");
                if (LOG.isInfoEnabled()) {
                    LOG.info("Input filename that you want create: ");
                }
                String newFileName = INPUT.nextLine();
                mainFile = new File(DEFAULT_PACKAGE_FILES + "\\" + newFileName);
                if (mainFile.createNewFile()) {
                    if (LOG.isInfoEnabled()) {
                        LOG.info("Fine created new file: " + newFileName);
                    }
                    break;
                }
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nCan't create file!");
                }
            } catch (IOException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nWrong file name!");
                }
            } catch (Exception e) {
                if (LOG.isFatalEnabled()) {
                    LOG.fatal("\nPlease, try again.");
                }
            }
        }
    }

    static void writeByBufferedReaderInInputStream() {
        createNewFile();
        writeToFileFromConsole();
    }

    private static void showMenuWriting() {
        System.out.println("\n\n");
        if (LOG.isInfoEnabled()) {
            LOG.info("Write only \"" + CLOSE_WRITING
                    + "\" in line: for exit from writing file.");
            LOG.info("Write message that you wanna push to your new file.\n\n");
        }
    }

    private static void writeToFileFromConsole() {
        try (OutputStream os = new FileOutputStream(mainFile.toString());
             BufferedOutputStream bos = new BufferedOutputStream(os);
             DataOutputStream out = new DataOutputStream(bos)) {

            String line;
            showMenuWriting();
            while (!(line = INPUT.nextLine()).equals(CLOSE_WRITING)) {
                showMenuWriting();
                line = line.trim() + "\r\n";
                out.write(line.getBytes());
            }
        } catch (IOException e) {
            if (LOG.isErrorEnabled()) {
                LOG.error("\nSomething wrong with file.");
            }
        } catch (Exception e) {
            if (LOG.isFatalEnabled()) {
                LOG.fatal("\nPlease, try again.");
            }
        }
    }

    static void showNameCurrentFile() {
        if (LOG.isInfoEnabled()) {
            LOG.info("\n\nCurrent file: " + mainFile.getName());
        }
    }

    static void showContentFile() {
        System.out.println("\n\n");
        if (!mainFile.toString().endsWith(WORST_FILES_ENDING)) {
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(mainFile),
                            StandardCharsets.UTF_8))) {

                StringBuilder sb = new StringBuilder();
                while (br.ready()) {
                    sb.append(br.readLine());
                    sb.append("\n");
                }
                if (LOG.isInfoEnabled()) {
                    LOG.info(sb.toString());
                }
            } catch (IOException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nWrong file name!");
                }
            } catch (Exception e) {
                if (LOG.isFatalEnabled()) {
                    LOG.fatal("\nPlease, try again.");
                }
            }
        } else {
            if (LOG.isErrorEnabled()) {
                LOG.error("Sorry, but .pdf file for show contents are bad idea!");
            }
        }
    }

    static void readByJustBufferedReader() throws IOException {
        setBufferSize();
        try (BufferedReader br = new BufferedReader(new FileReader(mainFile,
                StandardCharsets.UTF_8), sizeBuffer)) {
            long begin = System.nanoTime();
            int content = br.read();
            while (content != -1) {
                content = br.read();
            }
            long end = System.nanoTime();
            double result = (end - begin * 1.) / CONVERT_TO_SEC;
            if (LOG.isInfoEnabled()) {
                LOG.info("Current file: " + mainFile.getName());
                LOG.info("Size of buffer: " + sizeBuffer);
                LOG.info("(JustBufferedReader)Result in seconds: " + result);
            }
        }
    }

    private static void showMenu() {
        int size = files != null ? files.length : 0;
        System.out.println("\nMENU SETTING FILE:");
        for (int i = 0; i < size; i++) {
            System.out.println(" " + (i + 1) + " - Set file "
                    + files[i].getName() + ".");
        }
    }

    private static void changeFile(int index) {
        int size = files != null ? files.length : 0;
        //бо індекси на одиницю менше від меню вибору
        index--;
        for (int i = 0; i < size; i++) {
            if (index == i) {
                mainFile = new File(files[i].toString());
                if (LOG.isTraceEnabled()) {
                    LOG.trace("File successfully created!");
                }
                return;
            }
        }
        LOG.trace("Can't set file!");
    }

    static void setFile() {
        files = new File(DEFAULT_PACKAGE_FILES).listFiles();
        Arrays.sort(files != null ? files : new File[0]);
        int value;
        while (true) {
            try {
                System.out.println("\n");
                showMenu();
                if (LOG.isInfoEnabled()) {
                    LOG.info("Please, select menu point.");
                }
                String inputInt = INPUT.nextLine();
                value = Integer.parseInt(inputInt);
                changeFile(value);
                break;
            } catch (IllegalArgumentException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Size can't be less then zero");
                }
            } catch (Exception e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Please try again!\n");
                }
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Chose file(index): " + value);
        }
    }

    private static void inputSizeOfBuffer() {
        Integer value;
        while (true) {
            if(LOG.isInfoEnabled()) {
                LOG.info("Write size of buffer(or 0 if wanna set default size)");
            }
            String inputInt = INPUT.nextLine();
            try {
                value = Integer.parseInt(inputInt);
                if (value < 0) {
                    if (LOG.isTraceEnabled()) {
                        LOG.trace("Throw IllegalArgumentException");
                        throw new IllegalArgumentException();
                    }
                } else if (value == 0) {
                    if (LOG.isTraceEnabled()) {
                        LOG.trace("SizeBuffer = " + DEFAULT_SIZE_BUFFER);
                    }
                    sizeBuffer = DEFAULT_SIZE_BUFFER;
                    break;
                }
                sizeBuffer = value;
                break;
            } catch (IllegalArgumentException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Error\nSize can't be less then zero");
                }
            } catch (Exception e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Error\nPlease try again!\n");
                }
            }
        }
        System.out.println("\n");
    }

    private static void setBufferSize() {
        System.out.println("\nMENU FOR SETTING SIZE OF BUFFER:");
        inputSizeOfBuffer();
    }

    public static void run() {
        mainFile = DEFAULT_FILE_NAME;
        runMenuBuffer();
    }

    @Override
    public void close() {
        if (LOG.isInfoEnabled()) {
            LOG.info("Closed file");
        }
    }
}
