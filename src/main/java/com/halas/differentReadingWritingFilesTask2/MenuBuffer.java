package com.halas.differentReadingWritingFilesTask2;

import com.halas.menuView.Menu;
import com.halas.menuView.Functional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

final class MenuBuffer {
    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;
    private static final Scanner INPUT = new Scanner(System.in);
    private static final Logger LOG = LogManager.getLogger(MenuBuffer.class);

    private MenuBuffer() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - Set file.");
        menu.put("2", " 2 - Show name current filename.");
        menu.put("3", " 3 - Read by InputStream(without output content).");
        menu.put("4", " 4 - Read by BufferedReader in InputStream(without output content).");
        menu.put("5", " 5 - Read just BufferedReader(without output content).");
        menu.put("6", " 6 - Write by BufferedReader in InputStream to new created file.");
        menu.put("7", " 7 - Show contents current of file.");
        menu.put("8", " 8 - Back to task menu.");

        methodsMenu.put("1", DifferentWritingReadingFiles::setFile);
        methodsMenu.put("2", DifferentWritingReadingFiles::showNameCurrentFile);
        methodsMenu.put("3", DifferentWritingReadingFiles::readByByte);
        methodsMenu.put("4", DifferentWritingReadingFiles::readByBufferedReaderInInputStream);
        methodsMenu.put("5", DifferentWritingReadingFiles::readByJustBufferedReader);
        methodsMenu.put("6", DifferentWritingReadingFiles::writeByBufferedReaderInInputStream);
        methodsMenu.put("7", DifferentWritingReadingFiles::showContentFile);
        methodsMenu.put("8", Menu::runMenu);
    }

    private void showMenu() {
        System.out.println("\nMENU READING:");
        menu.values().forEach(System.out::println);
    }

    static void runMenuBuffer() {
        String key;
        MenuBuffer menuBuffer = new MenuBuffer();
        while (true) {
            try {
                System.out.println("\n");
                menuBuffer.showMenu();
                if (LOG.isInfoEnabled()) {
                    LOG.info("Please, select menu point.");
                }
                key = INPUT.nextLine();
                menuBuffer.methodsMenu.get(key).start();
            } catch (NullPointerException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nError\nPlease choose correct point.");
                }
            } catch (IOException e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("\nFile error!");
                }
            } catch (Exception e) {
                if (LOG.isFatalEnabled()) {
                    LOG.fatal("\nFatal error.");
                }
            }
        }
    }
}
